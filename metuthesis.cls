\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesClass{metuthesis}[2018/01/10 v0.1 Middle East Technical University Thesis Class]

% This class is based on METU Thesis Manual in effect in January 2018
% Was inspired from ttuthesis
% authors:
%      M. Sinan Hasanoglu, shasanoglu@gmail.com 

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}
\ProcessOptions\relax
\LoadClass[a4paper,12pt,twoside,onecolumn,openright]{memoir}

%\RequirePackage[T1]{fontenc}

% Set input encoding to utf-8 to make it easier for Turkish characters
\RequirePackage[utf8]{inputenc}

% Choose text font as Times New Roman and set a compatible math font 
\RequirePackage{newtxtext}
\RequirePackage{newtxmath}

% To be able to refer to the last page
\RequirePackage{lastpage}


% Setup margins
\setlrmarginsandblock{4cm}{2.5cm}{*}
\setulmarginsandblock{2.5cm}{4cm}{*}

% Default pagestyle is plain
% So only centered page numbers at the bottom of the page
\pagestyle{plain}

% 1.5 spacing between lines
\OnehalfSpacing*

%%% Settings related with lists of content
% Name of the ToC
\renewcommand{\contentsname}{Table of Contents}
% Center the titles and make them upper case
\renewcommand{\printtoctitle}[1]{\centering\bfseries\MakeUppercase #1}
\renewcommand{\printlottitle}[1]{\centering\bfseries\MakeUppercase #1}
\renewcommand{\printloftitle}[1]{\centering\bfseries\MakeUppercase #1}
% Add dotted leading lines to chapter entries
\renewcommand{\cftchapterdotsep}{\cftdotsep}
% Add Figure and Table names before their numbers in lists
\renewcommand*{\cftfigurename}{Figure\space}
\renewcommand*{\cfttablename}{Table\space}
% Add a CHAPTERS title before chepter entries occur
%% Also indent chapters until reerences
\newlength{\chapterstocindent}
\setlength{\chapterstocindent}{1.5em}
% Define a macro for removing the indentations
\newcommand{\reverttocindentations}{%
	\addtocontents{toc}{%
		\addtolength{\cftchapterindent}{-\chapterstocindent}
		\addtolength{\cftsectionindent}{-\chapterstocindent}
		\addtolength{\cftsubsectionindent}{-\chapterstocindent}
		\addtolength{\cftsubsubsectionindent}{-\chapterstocindent}
	}%
}
% Define macro endofchapters macro for a better user interface
\newcommand{\chaptersend}{\reverttocindentations}
\newcommand{\appendixend}{\reverttocindentations}

% Now make the changes after \mainmatter is issued
\addtodef{\mainmatter}{}{%
	\addtocontents{toc}{%
		\cftpagenumbersoff{part}
		\addtolength{\cftchapterindent}{\chapterstocindent}
		\addtolength{\cftsectionindent}{\chapterstocindent}
		\addtolength{\cftsubsectionindent}{\chapterstocindent}
		\addtolength{\cftsubsubsectionindent}{\chapterstocindent}
	}%
	\addcontentsline{toc}{part}{CHAPTERS}%
}
% Add a APPENDICES title before appendix entries occur
\addtodef{\appendix}{}{%
	\addtocontents{toc}{%
		\cftpagenumbersoff{part}
		\addtolength{\cftchapterindent}{\chapterstocindent}
		\addtolength{\cftsectionindent}{\chapterstocindent}
		\addtolength{\cftsubsectionindent}{\chapterstocindent}
		\addtolength{\cftsubsubsectionindent}{\chapterstocindent}	
	}%
	\addcontentsline{toc}{part}{APPENDICES}%
}


% Headings shall be numbered down to subsubsection
\maxsecnumdepth{subsubsection}

\newif\ifnonumchap
\nonumchapfalse

% Custom fomratting for section labels and names
\makeheadstyles{metu}{%
	\renewcommand*{\chapnamefont}{\huge\bfseries\centering\MakeUppercase}
	% Make non numbered chapter titles uppercase
	\renewcommand*{\printchapternonum}{\nonumchaptrue\bfseries\centering}
	\renewcommand*{\printchaptertitle}[1]{
		\ifnonumchap
			\MakeUppercase{##1}
		\else
			{\chaptitlefont ##1}
		\fi
		\nonumchapfalse
	}
	\setlength{\beforechapskip}{0pt}	
}

\headstyles{metu}

%%% Define macros for getting information related with the thesis
\newcommand{\titletr}[1]{\providecommand\metu@titletr{}\renewcommand{\metu@titletr}{#1}}
\titletr{You forgot to define the Turkish translation of your title}

\let\metuabstract\abstract
\let\endmetuabstract\endabstract
\renewcommand{\abstract}[1]{\providecommand\metu@abstract{}\renewcommand{\metu@abstract}{#1}}
\abstract{%
	If you can read this, you forgot to put an abstract in your thesis file.
}

\newcommand{\abstracttr}[1]{\providecommand\metu@abstracttr{}\renewcommand{\metu@abstracttr}{#1}}
\abstracttr{%
	If you can read this, you forgot to put an abstract translation in your thesis file.
}

\newcommand{\keywords}[1]{\providecommand\metu@keywords{}\renewcommand{\metu@keywords}{#1}}
\keywords{You forgot to define your keywords}

\newcommand{\keywordstr}[1]{\providecommand\metu@keywordstr{}\renewcommand{\metu@keywordstr}{#1}}
\keywordstr{You forgot to define translations of your keywords}

\newcommand{\name}[1]{\providecommand\metu@name{}\renewcommand{\metu@name}{#1}}
\name{(What's your name?)}

\newcommand{\surname}[1]{\providecommand\metu@surname{}\renewcommand{\metu@surname}{#1}}
\surname{(What's your surname?)}

\newcommand{\fullname}{\metu@name{} \metu@surname{}}

\newcommand{\metu@metu}{Middle East Technical University}

\newcommand{\graduateschool}[1]{\providecommand\metu@graduateschool{}\renewcommand{\metu@graduateschool}{#1}}
\graduateschool{(What's your graduate school?)}

\newcommand{\degree}[1]{\providecommand\metu@degree{}\renewcommand{\metu@degree}{#1}}
\degree{(What's your degree?)}

\newcommand{\shortdegree}[1]{\providecommand\metu@shortdegree{}\renewcommand{\metu@shortdegree}{#1}}
\shortdegree{(What's the short for your degree?)}

\newcommand{\shortdegreetr}[1]{\providecommand\metu@shortdegreetr{}\renewcommand{\metu@shortdegreetr}{#1}}
\shortdegreetr{(What's the short for your degree in Turkish?)}

\newcommand{\department}[1]{\providecommand\metu@department{}\renewcommand{\metu@department}{#1}}
\department{(What's your department?)}

\newcommand{\departmentfullname}[1]{\providecommand\metu@departmentfullname{}\renewcommand{\metu@departmentfullname}{#1}}
\departmentfullname{(What's your department's full name?)}

\newcommand{\departmentfullnametr}[1]{\providecommand\metu@departmentfullnametr{}\renewcommand{\metu@departmentfullnametr}{#1}}
\departmentfullnametr{(What's your department's full name in Turkish?)}

\newcommand{\graduationmonth}[1]{\providecommand\metu@graduationmonth{}\renewcommand{\metu@graduationmonth}{#1}}
\graduationmonth{(What's your graduation month?)}

\newcommand{\graduationmonthtr}[1]{\providecommand\metu@graduationmonthtr{}\renewcommand{\metu@graduationmonthtr}{#1}}
\graduationmonthtr{(What's your graduation month in Turkish?)}

\newcommand{\graduationyear}[1]{\providecommand\metu@graduationyear{}\renewcommand{\metu@graduationyear}{#1}}
\graduationyear{(What's your graduation year?)}

\newcommand{\dedication}[1]{\providecommand\metu@dedication{}\renewcommand{\metu@dedication}{#1}}
\dedication{(You did not write a dedication)}

\newcommand{\acknowledgments}[1]{\providecommand\metu@acknowledgments{}\renewcommand{\metu@acknowledgments}{#1}}
\acknowledgments{(You did not include acknowledgments)}

\newcommand{\approvaldate}[1]{\providecommand\metu@approvaldate{}\renewcommand{\metu@approvaldate}{#1}}
\approvaldate{(Approval Date)}


%%% Some multi lingual labels used in the frontmatter
\newcommand{\metu@supervisorlabel}{Supervisor}
\newcommand{\metu@cosupervisorlabel}{Co-supervisor}
\newcommand{\metu@abstractlabel}{Abstract}
\newcommand{\metu@keywordslabel}{Keywords}
%% And the Turkish versions of these labels
\newcommand{\metu@supervisorlabeltr}{Tez Yöneticisi}
\newcommand{\metu@cosupervisorlabeltr}{Ortak Tez Yöneticisi}
\newcommand{\metu@abstractlabeltr}{Öz}
\newcommand{\metu@keywordslabeltr}{Anahtar Kelimeler}

%%% Commands for supervisor, co-supervisor, approved by and committee members
\newcommand{\supervisor}[2]{%
	\providecommand\metu@supervisorname{}
	\renewcommand{\metu@supervisorname}{#1}
	\providecommand\metu@supervisoraff{}
	\renewcommand{\metu@supervisoraff}{#2}
	\newcommand{\supervisorname}{\metu@supervisorname}
	\newcommand{\supervisoraff}{\metu@supervisoraff}
}

%% We define a new if clause for checking the existance of co-supervisor
\newif\ifcosupervisor
% Initially it is set to false. If \cosupervisor is called it set to true
\cosupervisorfalse

\newcommand{\cosupervisor}[2]{%
	\providecommand\metu@cosupervisorname{}
	\renewcommand{\metu@cosupervisorname}{#1}
	\providecommand\metu@cosupervisoraff{}
	\renewcommand{\metu@cosupervisoraff}{#2}
	\newcommand{\cosupervisorname}{\metu@cosupervisorname}
	\newcommand{\cosupervisoraff}{\metu@cosupervisoraff}
	
	% set supervisor if clause to true
	\cosupervisortrue
}

%%% We define a new if clause for checking the existance of director
\newif\ifdirector
% Initially it is set to false. If \director is called it set to true
\directorfalse

\newcommand{\director}[2]{%
	\providecommand\metu@directorname{}
	\renewcommand{\metu@directorname}{#1}
	\providecommand\metu@directoraff{}
	\renewcommand{\metu@directoraff}{#2}
	
	% set if clause to true
	\directortrue
}

%%% We define a new if clause for checking the existance of head of dept
\newif\ifheadofdept
% Initially it is set to false. If \director is called it set to true
\headofdeptfalse

\newcommand{\headofdept}[2]{%
	\providecommand\metu@headofdeptname{}
	\renewcommand{\metu@headofdeptname}{#1}
	\providecommand\metu@headofdeptaff{}
	\renewcommand{\metu@headofdeptaff}{#2}
	
	% set if clause to true
	\headofdepttrue
}

%%% definitions for the committee members
%%% We define a new if clause for checking the existance of head of dept

\newif\ifcmi
\cmifalse
\newcommand{\cmi}[2]{%
	\providecommand\metu@cminame{}
	\renewcommand{\metu@cminame}{#1}
	\providecommand\metu@cmiaff{}
	\renewcommand{\metu@cmiaff}{#2}
	
	% set if clause to true
	\cmitrue
}

\newif\ifcmii
\cmiifalse
\newcommand{\cmii}[2]{%
	\providecommand\metu@cmiiname{}
	\renewcommand{\metu@cmiiname}{#1}
	\providecommand\metu@cmiiaff{}
	\renewcommand{\metu@cmiiaff}{#2}
	
	% set if clause to true
	\cmiitrue
}

\newif\ifcmiii
\cmiiifalse
\newcommand{\cmiii}[2]{%
	\providecommand\metu@cmiiiname{}
	\renewcommand{\metu@cmiiiname}{#1}
	\providecommand\metu@cmiiiaff{}
	\renewcommand{\metu@cmiiiaff}{#2}
	
	% set if clause to true
	\cmiiitrue
}

\newif\ifcmiv
\cmivfalse
\newcommand{\cmiv}[2]{%
	\providecommand\metu@cmivname{}
	\renewcommand{\metu@cmivname}{#1}
	\providecommand\metu@cmivaff{}
	\renewcommand{\metu@cmivaff}{#2}
	
	% set if clause to true
	\cmivtrue
}

\newif\ifcmv
\cmvfalse
\newcommand{\cmv}[2]{%
	\providecommand\metu@cmvname{}
	\renewcommand{\metu@cmvname}{#1}
	\providecommand\metu@cmvaff{}
	\renewcommand{\metu@cmvaff}{#2}
	
	% set if clause to true
	\cmvtrue
}

%%% Some predefined text and provided commands to change them
%% Title page text:
% The text you see after the title in title page
\newcommand{\posttitletext}[1]{\providecommand\metu@posttitletext{}\renewcommand{\metu@posttitletext}{#1}}
\posttitletext{A THESIS SUBMITTED TO\\ THE \MakeUppercase{\metu@graduateschool}\\ OF\\ \MakeUppercase{\metu@metu}}

% The presentation of the author name
\newcommand{\titlenametext}[1]{\providecommand\metu@titlenametext{}\renewcommand{\metu@titlenametext}{#1}}
\titlenametext{BY \vskip\baselineskip \MakeUppercase{\fullname}}

% the text you see after the author name in title page
\newcommand{\postnametext}[1]{\providecommand\metu@postnametext{}\renewcommand{\metu@postnametext}{#1}}
\postnametext{IN PARTIAL FULFILLMENT OF THE REQUIREMENTS\\ FOR\\ THE DEGREE OF \MakeUppercase{\metu@degree}\\ IN\\ \MakeUppercase{\metu@department}}

% The presentation of the date line at the bottom of the title page
\newcommand{\dateline}[1]{\providecommand\metu@dateline{}\renewcommand{\metu@dateline}{#1}}
\dateline{\MakeUppercase{\metu@graduationmonth} \metu@graduationyear}

%% Apprval page text. This text is later used by \makeapproval
\newcommand{\aptext}[1]{\providecommand\metu@aptext{}\renewcommand{\metu@aptext}{#1}}
\aptext{%
	\begin{center}
		Approval of the thesis:
		\vskip\baselineskip
		\bfseries\MakeUppercase{\thetitle}
	\end{center}
	\vskip\baselineskip
	submitted by {\bfseries\MakeUppercase{\fullname}} in partial fulfillment of the requirements for the degree of {\bfseries Doctor of Philosophy in \metu@departmentfullname, \metu@metu{}} by,
}

%% declaration
\newcommand{\declaration}[1]{\providecommand\metu@declaration{}\renewcommand{\metu@declaration}{#1}}
\declaration{%
	I hereby declare that all information in this document has been obtained and presented in accordance with academic rules and ethical conduct. I also declare that, as required by these rules and conduct, I have fully cited and referenced all material and results that are not original to this work.
}

%%% Title page styling and contents
\renewcommand{\maketitle}{%
	\begin{center}
		\vspace*{0.5cm}
		\MakeUppercase{\thetitle}
		\vfill
		\metu@posttitletext
		\vfill
		\metu@titlenametext
		\vfill
		\metu@postnametext
		\vfill
		\metu@dateline
	\end{center}
	\thispagestyle{empty}
}

%%% Approval page styling and content
\newcommand{\makeapproval}{%
	\metu@aptext	
	\vskip\baselineskip
	\begin{SingleSpace}
		\ifdirector
			\noindent\metu@directorname\\
			\metu@directoraff \hfill\parbox{3cm}{\hrulefill}	
		\fi
		\vskip\baselineskip
		
		\ifheadofdept
			\noindent\metu@headofdeptname\\
			\metu@headofdeptaff \hfill\parbox{3cm}{\hrulefill}	
		\fi
		\vskip\baselineskip
		
		\noindent\metu@supervisorname\\
		\metu@supervisorlabel, {\bfseries\metu@supervisoraff} \hfill\parbox{3cm}{\hrulefill}
		\vskip\baselineskip
		
		\ifcosupervisor
			\noindent\metu@cosupervisorname\\
			\metu@cosupervisorlabel, {\bfseries\metu@cosupervisoraff} \hfill\parbox{3cm}{\hrulefill}
		\fi
		
		\vskip 2\baselineskip
		
		\noindent\textbf{Examining Committee Members:}
		\vskip\baselineskip
		
		\ifcmi
			\noindent\metu@cminame\\
			\metu@cmiaff \hfill\parbox{3cm}{\hrulefill}	
		\fi
		\vskip\baselineskip
		
		\ifcmii
			\noindent\metu@cmiiname\\
			\metu@cmiiaff \hfill\parbox{3cm}{\hrulefill}	
		\fi
		\vskip\baselineskip	
		
		\ifcmiii
			\noindent\metu@cmiiiname\\
			\metu@cmiiiaff \hfill\parbox{3cm}{\hrulefill}	
		\fi
		\vskip\baselineskip		
		
		\ifcmiv
			\noindent\metu@cmivname\\
			\metu@cmivaff \hfill\parbox{3cm}{\hrulefill}	
		\fi
		\vskip\baselineskip	
		
		\ifcmv
			\noindent\metu@cmvname\\
			\metu@cmvaff \hfill\parbox{3cm}{\hrulefill}	
		\fi
		
		\vskip 2\baselineskip	
		
		\hfill \textbf{Date:} \parbox{3cm}{\centering\metu@approvaldate\hrule}	
	\end{SingleSpace}
	\thispagestyle{empty}
}

%%% Declaration page styling and content
\newcommand{\makedeclaration}{%
	\vspace*{\fill}
	
	\noindent\textbf{\metu@declaration}
	
	\vskip 2\baselineskip
	
	\newcommand{\namelabel}{Name, Last name}
	\newlength{\namelabelwidth}
	\settowidth{\namelabelwidth}{\namelabel{} :}
	\hfill\parbox{\the\namelabelwidth}{Name, Last name\hfill{}:}\hspace{1em}\fullname\hspace{1em}
	
	\vskip 1.5\baselineskip
	
	\hfill\parbox{\the\namelabelwidth}{Signature\hfill{}:}\hspace{1em}\phantom{\fullname}\hspace{1em}
	
	\vspace{3\baselineskip}
}

%%% Abstract page styling and content
\newcommand{\makeabstract}{%
	\begin{center}
		\textbf{\MakeUppercase{\metu@abstractlabel}}
		\addcontentsline{toc}{chapter}{\metu@abstractlabel}
		
		\vskip 2\baselineskip
		
		\textbf{\MakeUppercase{\thetitle}}
		
		\vskip 2\baselineskip
		
		\begin{tabular}{ll}
			\multicolumn{2}{c}{\metu@surname, \metu@name} \\ 
			\multicolumn{2}{l}{\metu@shortdegree, \metu@departmentfullname} \\
			\metu@supervisorlabel & : \metu@supervisorname \\ 
			\ifcosupervisor
				\metu@cosupervisorlabel	& : \metu@cosupervisorname\\ 
			\fi
		\end{tabular} 
	
		\vskip\baselineskip
		
		\metu@graduationmonth{} \metu@graduationyear{}, \pageref{LastPage} pages
	\end{center}

	\vskip\baselineskip

	\noindent\metu@abstract
	
	\vskip\baselineskip
	
	\noindent\metu@keywordslabel{}: \metu@keywords
}

%%% Turkish Abstract page styling and content
%% This is basically repetition of the above code block with turkish alternatives of the macros
\newcommand{\makeabstracttr}{%
	\begin{center}
		\textbf{\MakeUppercase{\metu@abstractlabeltr}}
		\addcontentsline{toc}{chapter}{\metu@abstractlabeltr}
		
		\vskip 2\baselineskip
		
		\textbf{\MakeUppercase{\metu@titletr}}
		
		\vskip 2\baselineskip
		
		\begin{tabular}{ll}
			\multicolumn{2}{c}{\metu@surname, \metu@name} \\ 
			\multicolumn{2}{l}{\metu@shortdegreetr, \metu@departmentfullnametr} \\
			\metu@supervisorlabeltr & : \metu@supervisorname \\ 
			\ifcosupervisor
			\metu@cosupervisorlabeltr	& : \metu@cosupervisorname\\ 
			\fi
		\end{tabular} 
		
		\vskip\baselineskip
		
		\metu@graduationmonthtr{} \metu@graduationyear{}, \pageref{LastPage} sayfa
	\end{center}
	
	\vskip\baselineskip
	
	\noindent\metu@abstracttr
	
	\vskip\baselineskip
	
	\noindent\metu@keywordslabeltr{}: \metu@keywordstr
}

%%% Dedication page styling and content
\newcommand{\makededication}{%
	\addcontentsline{toc}{chapter}{Dedication}
	\vspace*{\fill}
	\metu@dedication
	\vspace*{\fill}		
}

%%% Acknowledgements page styling and content
\newcommand{\makeacknowledgements}{%
	\begin{center}
		\textbf{ACKNOWLEDGMENTS}
		\addcontentsline{toc}{chapter}{Acknowledgements}
	\end{center}

	\vskip 2\baselineskip

	\noindent\metu@acknowledgments

}

%%% Some initialization for the vita page
\newcommand{\vita}{%
	\begin{center}
		\textbf{CURRICULUM VITAE}
		\addcontentsline{toc}{chapter}{Curriculum Vitae}
	\end{center}
}


%%%
%%% All done. Let memoir finish its setup now.
%%%

\checkandfixthelayout

\endinput
