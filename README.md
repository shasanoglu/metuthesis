# metuthesis

LaTeX document class for Middle East Technical University (METU) Thesis with sample usage. 
In other words, LaTeX Thesis template for Middle East Technical University (METU)

`metuthesis` is based on METU Thesis Manual in effect in January 2018. 
It hasn't been approved by any graduate school. If you run into problems fast corrections can be made until end of 2018.
At that point I hopefully will have defended my dissertation and therefore will have 
the document class approved by at least one graduate school.

The document class has been tested with:

1. Tex Live 2016 | Windows 10

## How can you help?

1. 	I tried but couldn't get the document class approved by graduate schools (they didn't reply my e-mails). 
	If you have contacts and/or time you can help us get it approved. My worst case plan is to have it approved
	when I submit my thesis, and correct the errors not only for myself but for everyone using `metuthesis`.
2.	I only tested this class in my own environment. 
	If you successfully use the class in different platforms or LaTeX distros (such as MiKTeX), please report results.
3.	If you find problems/bugs please report (maybe even create a pull request :) ).